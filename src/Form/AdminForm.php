<?php

namespace Drupal\lemonpie\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class AdminForm extends ConfigFormBase {

  protected $lemonpie_config;

  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
    $this->lemonpie_config = $this->config('lemonpie.config');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['cookies_config'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Configuration'),
    ];

    $form['cookies_config']['selector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Auto open panel with hashtag'),
      '#default_value' => $this->getDefaultConfigValue('selector', '#tarteaucitron'),
      '#required' => TRUE,
    ];

    $form['cookies_config']['cookie_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cookie name'),
      '#default_value' => $this->getDefaultConfigValue('cookie_name', 'tarteaucitron'),
      '#required' => TRUE,
    ];

    $form['cookies_config']['high_privacy'] = [
      '#type' => 'radios',
      '#title' => $this->t('Disable implicit consent'),
      '#default_value' => $this->getDefaultConfigValue('high_privacy', 0),
      '#options' => [
        1 => $this->t('Yes'),
        0 => $this->t('No'),
      ],
      '#required' => TRUE,
    ];

    $form['cookies_config']['external_css'] = [
      '#type' => 'radios',
      '#title' => $this->t('Use custom css'),
      '#default_value' => $this->getDefaultConfigValue('external_css', 0),
      '#options' => [
        1 => $this->t('Yes'),
        0 => $this->t('No'),
      ],
      '#required' => TRUE,
    ];

    $form['cookies_config']['accept_all_cta'] = [
      '#type' => 'radios',
      '#title' => $this->t('Display accept all button'),
      '#default_value' => $this->getDefaultConfigValue('accept_all_cta', 1),
      '#options' => [
        1 => $this->t('Yes'),
        0 => $this->t('No'),
      ],
      '#required' => TRUE,
    ];


    $form['cookies_config']['position'] = [
      '#type' => 'select',
      '#title' => $this->t('Position'),
      '#default_value' => $this->getDefaultConfigValue('position', 'top'),
      '#options' => [
        'top' => $this->t('Top'),
        'bottom' => $this->t('Bottom'),
      ],
      '#required' => TRUE,
    ];

    $form['cookies_config']['ad_blocker'] = [
      '#type' => 'radios',
      '#title' => $this->t('Display a message if an Ad Blocker is detected'),
      '#default_value' => $this->getDefaultConfigValue('ad_blocker', 0),
      '#options' => [
        1 => $this->t('Yes'),
        0 => $this->t('No'),
      ],
      '#required' => TRUE,
    ];

    $form['cookies_config']['show_alert_small'] = [
      '#type' => 'radios',
      '#title' => $this->t('Display small banner at the bottom right of page'),
      '#default_value' => $this->getDefaultConfigValue('show_alert_small', 0),
      '#options' => [
        1 => $this->t('Yes'),
        0 => $this->t('No'),
      ],
      '#required' => TRUE,
    ];

    $form['cookies_config']['cookies_list'] = [
      '#type' => 'radios',
      '#title' => $this->t('Display the installed cookies list'),
      '#default_value' => $this->getDefaultConfigValue('cookies_list', 1),
      '#options' => [1 => 'oui', 0 => 'non'],
      '#required' => TRUE,
    ];

    $form['cookies_config']['remove_credit'] = [
      '#type' => 'radios',
      '#title' => $this->t('Remove link to the credits'),
      '#default_value' => $this->getDefaultConfigValue('remove_credit', 0),
      '#options' => [
        1 => $this->t('Yes'),
        0 => $this->t('No'),
      ],
      '#required' => TRUE,
    ];

    $form['cookies_config']['do_not_track'] = [
      '#type' => 'radios',
      '#title' => $this->t('Handle the <em>Do Not Track</em> requests ?'),
      '#default_value' => $this->getDefaultConfigValue('do_not_track', 0),
      '#options' => [
        1 => $this->t('Yes'),
        0 => $this->t('No'),
      ],
      '#required' => TRUE,
    ];

    $form['cookies_config']['show_more_info'] = [
      '#type' => 'radios',
      '#title' => $this->t('Display the more info link in the popin ?'),
      '#default_value' => $this->getDefaultConfigValue('show_more_info', 0),
      '#options' => [
        1 => $this->t('Yes'),
        0 => $this->t('No'),
      ],
      '#required' => TRUE,
    ];

    $form['cookies_config']['privacy_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Privacy url'),
      '#default_value' => $this->getDefaultConfigValue('privacy_url', ''),
      '#description' => t('Path to Cookies policy page on the site. e.g <strong>/cookiespolicy</strong>'),
      '#required' => FALSE,
    ];


    $form['cookies_config']['debug'] = [
      '#type' => 'radios',
      '#title' => $this->t('Debug mode'),
      '#description' => $this->t('The cookie will be unset on every page load to design or debug purpose'),
      '#default_value' => $this->getDefaultConfigValue('debug', 0),
      '#options' => [
        1 => $this->t('Yes'),
        0 => $this->t('No'),
      ],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * @param $config_key
   * @param $default
   *
   * @return array|int|mixed|null
   */
  private function getDefaultConfigValue($config_key, $default) {
    if (!is_null($this->lemonpie_config->get($config_key))) {
      if (is_bool($this->lemonpie_config->get($config_key))) {
        if ($this->lemonpie_config->get($config_key)) {
          return 1;
        }
        else {
          return 0;
        }
      }
      return $this->lemonpie_config->get($config_key);
    }
    return $default;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lemonpie_admin';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('lemonpie.config');
    $values = $form_state->getValues();

    foreach (lemonpie_get_config_keys() as $key) {
      if (isset($values[$key])) {
        $value = $values[$key];
        if ($value === '0') {
          $value = FALSE;
        }
        elseif ($value === '1') {
          $value = TRUE;
        }
        $config->set($key, $value);
      }
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'lemonpie.config',
    ];
  }
}
