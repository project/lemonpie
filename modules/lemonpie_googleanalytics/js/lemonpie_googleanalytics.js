(function ($, Drupal) {
  Drupal.behaviors.lemonpie_googleanalytics = {
    attach: function (context, settings) {
      if (drupalSettings.lemonpie.ga_id) {
        tarteaucitron.user.analyticsUa = drupalSettings.lemonpie.ga_id;
      }
      (tarteaucitron.job = tarteaucitron.job || []).push('analytics');
    }
  }
})(jQuery, Drupal);
