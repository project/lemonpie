<?php

namespace Drupal\lemonpie_googleanalytics\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class AdminForm.
 */
class AdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'lemonpie_googleanalytics.admin',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('lemonpie_googleanalytics.admin');
    $form['ga_id'] = [
      '#type' => 'textfield',
      '#description' => $this->t('This ID is unique to each site you want to track separately, and is in the form of UA-xxxxxxx-yy. To get a Web Property ID, <a href=":analytics">register your site with Google Analytics</a>, or if you already have registered your site, go to your Google Analytics Settings page to see the ID next to every site profile. <a href=":webpropertyid">Find more information in the documentation</a>.', [
        ':analytics' => 'https://marketingplatform.google.com/about/analytics/',
        ':webpropertyid' => Url::fromUri('https://developers.google.com/analytics/resources/concepts/gaConceptsAccounts', ['fragment' => 'webProperty'])
          ->toString(),
      ]),
      '#maxlength' => 20,
      '#placeholder' => 'UA-',
      '#required' => TRUE,
      '#size' => 20,
      '#title' => $this->t('Web Property ID'),
      '#default_value' => $config->get('ga_id'),
    ];


    $form['page_visibility_settings'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Disable on theses pages'),
      '#default_value' => !empty($config->get('visibility.page_visibility_settings')) ? $config->get('visibility.page_visibility_settings') : '',
      '#description' => $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", [
        '%blog' => '/blog',
        '%blog-wildcard' => '/blog/*',
        '%front' => '<front>',
      ]),
      '#rows' => 10,
    ];

    $form['role_visibility_settings'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Display for roles'),
      '#default_value' => !empty($config->get('visibility.role_visibility_settings')) ? $config->get('visibility.role_visibility_settings') : ['anonymous'],
      '#options' => array_map('\Drupal\Component\Utility\Html::escape', user_role_names()),
      '#description' => $this->t('If none of the roles are selected, all users will be tracked. If a user has any of the roles checked, that user will be tracked (or excluded, depending on the setting above).'),
      ];


    // @todo add aditionnal ga push
//
//    $form['additionnal_ga_push'] = [
//
//    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('lemonpie_googleanalytics.admin');

    $config->set('ga_id', $form_state->getValue('ga_id'));
    $config->set('visibility.page_visibility_settings', $form_state->getValue('page_visibility_settings'));
    $config->set('visibility.role_visibility_settings', $form_state->getValue('role_visibility_settings'));

    $config->save();
  }

}
