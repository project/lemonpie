(function ($, Drupal) {
  Drupal.behaviors.lemonpie = {
    attach: function (context, settings) {
      var config = drupalSettings.lemonpie.config;


      // Debug
      if (config.debug) {
        document.cookie = config.cookie_name + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
      }

      // Init tarteaucitron
      var tarteaucitronConfig = {
        'hashtag': config.selector,
        'cookieName': config.cookie_name,
        'orientation': config.position,
        "showAlertSmall": config.show_alert_small,
        "cookieslist": config.cookies_list,

        "adblocker": config.ad_blocker,
        "AcceptAllCta": config.accept_all_cta,
        'highPrivacy': config.high_privacy,
        "handleBrowserDNTRequest": config.do_not_track,

        "removeCredit": config.remove_credit,
        "moreInfoLink":  config.show_more_info,
        "useExternalCss": config.external_css,
      };

      if (config.privacy_url) {
        tarteaucitronConfig.privacyUrl = config.privacy_url
      }
      tarteaucitron.init(tarteaucitronConfig);
    }
  }
})(jQuery, Drupal);
